\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[naustrian]{babel}
\usepackage{url}
\usepackage{lmodern}
\usepackage[font=footnotesize,labelformat=empty]{caption}
\usepackage{listings}


\lstset{
  basicstyle=\footnotesize\ttfamily,
  breakatwhitespace=true,
  columns=fullflexible,
  keepspaces=true,
  breaklines=true,
  tabsize=2,
  showstringspaces=false,
  extendedchars=true
}

% fonts
\setbeamerfont{framesubtitle}{size=\normalsize}
\setbeamercolor{framesubtitle}{fg=black,bg=white}

% pdf metadata
\hypersetup{pdftitle={\inserttitle},
  pdfsubject={\inserttitle},
  pdfauthor={\insertauthor},
  pdfkeywords={\inserttitle},
  pdfpagelabels={false},
%  pdfpagemode={FullScreen}
}

% references style
\bibliographystyle{plain}

% show the key instead of some image
\setbeamertemplate{bibliography item}[text]

\usetheme{Singapore}

\title{btrfs}
\subtitle{Eine Einführung}
\author[Florian Preinstorfer]{
  \parbox[t]{4cm}{\hfill Florian Preinstorfer\hfill\ \\\hbox{} \hfill{\footnotesize \url{http://nblock.org}}\hfill\hbox{}}
}

\date{Linuxwochen Linz\\30.05.2015}
\titlegraphic{
  \includegraphics[height=1cm]{img/btrfs-logo}\\
  \vspace{0.2cm}
  \includegraphics[height=0.5cm]{img/by-sa}

  \tiny{This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Austria license (CC-BY-SA).}
}

\begin{document}

% title
\frame[plain]{\titlepage}

% toc
\begin{frame}{Inhalt}
  \tableofcontents
\end{frame}

\section{Einleitung}
\begin{frame}{Historie}
  \begin{itemize}
    \item[2007] Usenix 07: Vorstellung einer Copy-On-Write freundlichen B-tree Variante \cite{shorthistory}.
    \item[2007] Chris Mason kündigt btrfs auf der LKML an \cite{announce}.
    \item[2009] Aufnahme in Linux 2.6.29 \cite{announce, changelog}.
    \item[20xx] Support in Arch, Debian, Fedora, Ubuntu, \dots
    \item[2012] Kommerzielle Unterstützung in Oracle Unbreakable Linux und SLES 11 SP2 \cite{changelog}.
  \end{itemize}
\end{frame}

\begin{frame}{Aktueller Status}
  \begin{itemize}
    \item Seit Linux 3.13: \textit{The filesystem disk format is no longer unstable, and it's not
        expected to change unless there are strong reasons to do so.} \cite{kerneltree}
    \item Viele Änderungen mit jeder neuen Kernel Version \cite{changelog}.
    \item Lizenz: GPLv2
  \end{itemize}
\end{frame}

% created with gitstats
% http://gitstats.sourceforge.net/
\begin{frame}{Projektaktivität}
  \framesubtitle{Kernelmodul, LOC: $\sim${}86k}
  \begin{figure}
    \includegraphics[width=0.95\textwidth]{img/kernel_commits_by_year_month.png}
    \caption{Commits pro Jahr und Monat.}
  \end{figure}
\end{frame}

\begin{frame}{Projektaktivität}
  \framesubtitle{Kernelmodul, LOC: $\sim${}86k}
  \begin{figure}
    \includegraphics[width=0.8\textwidth]{img/kernel_commits_by_author.png}
    \caption{Commits pro Entwickler.}
  \end{figure}
\end{frame}

\begin{frame}{Projektaktivität}
  \framesubtitle{Userland tools, LOC: $\sim${}60k}
  \begin{figure}
    \includegraphics[width=0.95\textwidth]{img/progs_commits_by_year_month.png}
    \caption{Commits pro Jahr und Monat.}
  \end{figure}
\end{frame}

\begin{frame}{Projektaktivität}
  \framesubtitle{Userland tools, LOC: $\sim${}60k}
  \begin{figure}
    \includegraphics[width=0.8\textwidth]{img/progs_commits_by_author.png}
    \caption{Commits pro Entwickler.}
  \end{figure}
\end{frame}

\section{Features und Demos}
% + Mostly self-healing in some configurations due to the nature of copy on write
% - Online defragmentation
% + Online volume growth and shrinking
% + Online block device addition and removal
% + Online balancing (movement of objects between block devices to balance load)
% + Offline filesystem check
% - Online data scrubbing for finding errors and automatically fixing them for files with redundant copies
% + RAID 0, RAID 1, RAID 5, RAID 6 and RAID 10
% + Subvolumes (one or more separately mountable filesystem roots within each disk partition)
% + Transparent compression (zlib and LZO)
% + Snapshots (read-only[26] or copy-on-write clones of subvolumes)
% - File cloning (copy-on-write on individual files, or byte ranges thereof)
% - Checksums on data and metadata (CRC-32C[27])
% - In-place conversion (with rollback) from ext3/4 to Btrfs[28]
% - File system seeding[29] (Btrfs on read-only storage used as a copy-on-write backing for a writeable Btrfs)
% + Block discard support (reclaims space on some virtualized setups and improves wear leveling on SSDs with TRIM)
% - Send/receive (saving diffs between snapshots to a binary stream)[30]
% - Hierarchical per-subvolume quotas[31]
% + Data deduplication[4][32] -> neuer Kernel ?

\begin{frame}{Copy-On-Write}
  \begin{itemize}
    \item Geänderte Blöcke werden nicht überschrieben, sondern in einem freien
      Speicherbereich abgelegt. Anschließend wird der Verweis auf den neuen
      Block in den Metadaten aktualisiert \cite{bacik}.
    \item Kein Journal nötig.
    \item Dateisystemkonsistenz wird sichergestellt.
  \end{itemize}
\end{frame}

\begin{frame}{Transparente Kompression}
  \begin{itemize}
    \item Automatische Kompression auf Dateiebene, wenn Speicherplatz eingespart werden kann \cite{bacik}.
    \item Verfügbare Kompressionsalgorithmen: ZLIB oder LZO
    \item Automatische Kompression: \texttt{mount -o compress}
    \item Erzwungene Kompression: \texttt{mount -o compress-force}
  \end{itemize}
\end{frame}

\begin{frame}{Online Resize*}
  \begin{itemize}
    \item Ein btrfs Dateisystem kann im Betrieb sowohl \emph{verkleinert} als
      auch \emph{vergrößert} werden.
    \item Auch für mehrere Festplatten möglich.
    \item \texttt{btrfs filesystem resize \dots}
  \end{itemize}
\end{frame}

\begin{frame}{Festplatten hinzufügen/entfernen*}
  \begin{itemize}
    \item Einem bestehenden btrfs Dateisystem können zur Laufzeit Festplatten hinzugefügt/entfernt
      werden.
    \item Hinzufügen: Neue Blöcke werden nach und nach auf der neuen Festplatte allokiert.
    \item Entfernen: Nicht redundante Daten werden vorab auf bestehende Festplatten kopiert.
    \item Daten werden \emph{nicht} verteilt (balance).
    \item \texttt{btrfs device add/delete \dots}
  \end{itemize}
\end{frame}

\begin{frame}{Online Balance*}
  \begin{itemize}
    \item Daten werden gelesen und je nach Allokationsmodus neu auf dem btrfs Dateisystem verteilt.
    \item Anwendungsfall: Eine Festplatte wurde hinzugefügt/entfernt.
    \item \texttt{btrfs balance <path>}
  \end{itemize}
\end{frame}

\begin{frame}{RAID*}
  \begin{itemize}
    \item btrfs bietet Unterstützung für RAID 0, 1, 10.
    \item Seit Linux 3.9: Initiale Unterstützung für RAID 5, 6 \cite{raidsupport}.
    \item „Recovery and rebuild“ erst seit Linux 3.19 möglich.
    \item Das RAID Level kann für Daten (-d) und Metadaten (-m) separat angegeben
      werden\footnote{Online btrfs disk usage calculator:
      \url{http://carfax.org.uk/btrfs-usage/index.html}}.
    \item \texttt{mkfs.btrfs -m <metadata raid> -d <data raid> \dots}
  \end{itemize}
\end{frame}

\begin{frame}{Prüfsummen}
  \begin{itemize}
    \item btrfs berechnet Prüfsummen für Daten und Metadaten.
    \item Bei jedem Lesezugriff wird geprüft, ob die Daten korrekt gelesen werden konnten.
    \item Bei Lesefehlern versucht btrfs die defekten Blöcke mittels unbeschädigter Kopien zu reparieren.
    \item Algorithmus: derzeit nur crc32c
  \end{itemize}
\end{frame}

\begin{frame}{Scrubbing}
  \begin{itemize}
    \item Fehler im Dateisystem werden mit Hilfe von Prüfsummen und redundanten Kopien repariert.
    \item Srubbing passiert im Hintergrund und kann sehr lange dauern.
    \item \texttt{btrfs scrub <start|cancel|resume|status> <path>}
  \end{itemize}
\end{frame}

\begin{frame}{Subvolumes*}
  \begin{itemize}
    \item Subvolumes sind Namespaces innerhalb eines Dateisystems.
    \item Sie verhalten sich wie Verzeichnisse (keine devices!).
    \item Subvolumes können gemounted werden.
    \item Das default Subvolume (0) ist immer vorhanden.
    \item \texttt{btrfs subvolume <command> \dots}
  \end{itemize}
\end{frame}

\begin{frame}{Snapshots*}
  \begin{itemize}
    \item Snapshots sind Subvolumes.
    \item Snapshots können schreibgeschützt werden.
    \item Typische Anwendungsfälle: Backup, Experimente, \dots
    \item \texttt{btrfs subvolume snapshot \dots}
  \end{itemize}
\end{frame}

\begin{frame}{Seed devices}
  \begin{itemize}
    \item Schreibgeschütztes „Image“ für ein btrfs Dateisystem.
    \item Änderungen landen nur im beschreibbaren btrfs Dateisystem.
    \item Seed devices können jederzeit entfernt werden.
    \item \texttt{btrfstune -S 1 <seed-device>}
  \end{itemize}
\end{frame}

\begin{frame}{Support für SSDs}
  % SSD (Flash storage) awareness (TRIM/Discard for reporting free blocks for reuse) and optimizations
  % (e.g. avoiding unnecessary seek optimizations, sending writes in clusters, even if they are from
  % unrelated files. This results in larger write operations and faster write throughput)
  \begin{itemize}
    % enable via -o ssd or automatically since 2.6.31
    \item Optimierungen für SSDs werden automatisch aktiviert.
    \item Unterstützung für TRIM ist standardmäßig deaktiviert.
    \begin{itemize}
      \item Aktivieren via \texttt{mount -o discard}
      \item Manuell via \texttt{fstrim(8)}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Offline Dateisystemprüfung}
  % - btrfs scrub to detect issues on live filesystems
  % - look at btrfs detected errors in syslog (look at Marc's blog above on how to use sec.pl to do
  %   this)
  % - mount -o ro,recovery to mount a filesystem with issues
  % - btrfs-zero-log might help in specific cases. Go read Btrfs-zero-log
  % - btrfs restore will help you copy data off a broken btrfs filesystem.
  % - btrfs check --repair, aka btrfsck is your last option if the ones above have not worked.
  \begin{itemize}
    \item Ja, mittlerweile gibt es \texttt{btrfs check}\footnote{\texttt{btrfsck} wurde von
      \texttt{btrfs check} abgelöst.}.
    \item Ja, es kann auch ein kaputtes btrfs Dateisystem reparieren.
    \item Ist als letzter Ausweg vor Datenverlust gedacht.
    \item \texttt{btrfs check [options] /dev/<umounted-device>}
  \end{itemize}
\end{frame}

\section{Erfahrungen}
\begin{frame}{btrfs und ich in den letzten 4-5 Jahren}
    \begin{itemize}
      \item 3.9.x: btrfs verliert eine Disk in meiner NAS \(\rightarrow\) btrfs scrub notwendig
      \item 3.11.3: Kernel bug in btrfs balance \cite{balancebug}
      \item 3.13.x: Disks können nicht aus einem RAID entfernt werden.
      \item aktuell (seit 3.18.x): Hängt beim Löschen von Dateien \cite{crashbug}
    \end{itemize}
\end{frame}

\section{Fazit}
\begin{frame}{Fazit}
  \begin{itemize}
    \item Gute Unterstützung in vielen Distributionen.
    \item Tools sind nicht ausgereift.
    \item Schwerwiegende Probleme während meiner Experimente.
    \item btrfs wird – hoffentlich bald – das „next generation filesystem für Linux“.
    \item Einfach mal ausprobieren.
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]{Referenzen}
  \begin{tiny}
    \bibliography{references}
  \end{tiny}
\end{frame}

\begin{frame}[fragile]{Freier Speicherplatz \cite{wiki}}
    \begin{itemize}
      \item Bisher mühselige Berechnung mit:
      \begin{itemize}
        \item \texttt{btrfs filesystem show}
        \item \texttt{btrfs filesystem df}
      \end{itemize}
    \end{itemize}
    \begin{lstlisting}
$ sudo btrfs fi show
Label: none  uuid: 12345678-1234-5678-1234-1234567890ab
  Total devices 2 FS bytes used 304.48GB
  devid    1 size 427.24GB used 197.01GB path /dev/sda1
  devid    2 size 465.76GB used 197.01GB path /dev/sdc1

$ sudo btrfs fi df /mnt
Metadata, single: total=18.00GB, used=6.10GB
Data, single: total=376.00GB, used=298.37GB
System, single: total=12.00MB, used=40.00KB
    \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Freier Speicherplatz}
    \begin{itemize}
      \item Seit btrfs-progs 3.18 gibt es \texttt{btrfs filesystem usage}
    \end{itemize}
    \begin{lstlisting}
$ sudo btrfs fi usage /mnt
Overall:
  Device size:        80.00GiB
  Device allocated:   32.02GiB
  Device unallocated: 47.98GiB
  Used:               29.33GiB
  Free (estimated):   24.34GiB  (min: 24.34GiB)
  Data ratio:         2.00
  Metadata ratio:     2.00
  Global reserve:     16.00MiB  (used: 0.00B)
    \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Freier Speicherplatz}
    \begin{lstlisting}
$ sudo btrfs fi usage /mnt    (continued)
Data,RAID1: Size:15.00GiB, Used:14.65GiB
  /dev/sdc1     15.00GiB
  /dev/sdd1     15.00GiB

Metadata,RAID1: Size:1.00GiB, Used:15.59MiB
  /dev/sdc1     1.00GiB
  /dev/sdd1     1.00GiB

System,RAID1: Size:8.00MiB, Used:16.00KiB
  /dev/sdc1     8.00MiB
  /dev/sdd1     8.00MiB

Unallocated:
  /dev/sdc1     23.99GiB
  /dev/sdd1     23.99GiB
    \end{lstlisting}
\end{frame}

\end{document}

% vim: tabstop=2 expandtab shiftwidth=2 softtabstop=2 autoindent
